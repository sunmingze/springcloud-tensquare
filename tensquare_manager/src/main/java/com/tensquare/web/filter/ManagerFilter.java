package com.tensquare.web.filter;



import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.stereotype.Component;
/**
 * 类名称:ManagerFilter
 * 类描述:  zuul 网关过滤器
 *
 * @author SunMingZ
 * @Description：TODO
 * @date：2020/7/6
 * Version 1.0
 */
@Component
public class ManagerFilter extends ZuulFilter {

    /**
     * 方法名: filterType
     * 方法描述:  过滤器执行的类型
     * pre 代表在执行之前执行, 一般使用pre 进行之前过滤
     * post 代表在当前操作之后过滤
     * 修改日期: 2019/2/23 14:10
     * @param
     * @return java.lang.String
     * @author taohongchao
     * @throws
     */
    @Override
    public String filterType() {
        return "pre";
    }

    /**
     * 方法名: filterOrder
     * 方法描述: 多个过滤器执行顺序, 数字越小,代表越前执行
     * 修改日期: 2019/2/23 14:12
     * @param
     * @return int
     * @author taohongchao
     * @throws
     */
    @Override
    public int filterOrder() {
        return 0;
    }


    /**
     * 方法名: shouldFilter
     * 方法描述: 当前过滤器是否开启, true代表开启, false代表关闭该过滤器
     * 修改日期: 2019/2/23 14:13
     * @param
     * @return boolean
     * @author taohongchao
     * @throws
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * 方法名: run
     * 方法描述:  过滤器内执行的操作, return代表任何Object的值(包括null),都代表继续执行
     * 而调用 setSendZuulResponse(false) 表示不再继续执行
     * 修改日期: 2019/2/23 14:13
     * @param
     * @return java.lang.Object
     * @author taohongchao
     * @throws
     */
    @Override
    public Object run() throws ZuulException {
        System.out.println("经过后台过滤器了");
        return null;
    }
}

