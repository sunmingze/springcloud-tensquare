package com.tensquare.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * 类名称:ManagerApplication 类描述:zuul 服务的启动类
 *
 * @author: taohongchao 创建时间:2019/2/23 11:48 Version 1.0
 */
@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
public class ManagerApplication {

  public static void main(String[] args) {
    SpringApplication.run(ManagerApplication.class);
  }
}
