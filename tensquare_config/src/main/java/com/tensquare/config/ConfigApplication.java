package com.tensquare.config;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * 类名称:ConfigApplication
 * 类描述: 配置中心的启动类
 * @author SunMingZ
 * @Description：TODO
 * @date：2020/7/6
 * Version 1.0
 */
@SpringBootApplication
@EnableConfigServer
public class ConfigApplication {

    public static void main(String[] args){
        SpringApplication.run(ConfigApplication.class);
    }
}

