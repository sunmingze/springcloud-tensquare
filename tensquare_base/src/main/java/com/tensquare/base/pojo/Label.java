package com.tensquare.base.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/** Created by me on 2019/6/25. */
@Entity
@Table(name = "tb_label")
public class Label {

  @Id
  @Column(name = "id")
  private String id;

  @Column(name = "labelname")
  private String labelname;

  @Column(name = "state")
  private String state;

  @Column(name = "count")
  private Long count;

  @Column(name = "fans")
  private Long fans;

  @Column(name = "recommend")
  private String recommend;

  // 省略get set方法

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getLabelname() {
    return labelname;
  }

  public void setLabelname(String labelname) {
    this.labelname = labelname;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public Long getCount() {
    return count;
  }

  public void setCount(Long count) {
    this.count = count;
  }

  public Long getFans() {
    return fans;
  }

  public void setFans(Long fans) {
    this.fans = fans;
  }

  public String getRecommend() {
    return recommend;
  }

  public void setRecommend(String recommend) {
    this.recommend = recommend;
  }
}
